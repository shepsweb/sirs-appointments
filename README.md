# Sirs-Appointments #

Provides migrations, seeds, models, events, artisan commands, and jobs to support appointment management.

### Installation ###
Add the following to your composer.json

```
#!php
...
  "require": {
    ...
    "sirs/appointments": "^11.0"
   }
```
*Note: use version 1.0.0 for Laravel 5.0.x*

### What's included ###

* Migrations for tables
    - appointment_statuses
    - appointment_types
    - appointments
* Seeds of common statuses and types
* Models
    - Appointment
    - AppointmentType
    - AppointemntStatus
* Traits
    - AppointmentAttendeeTraits
    - AppointmentStaffAttendeeTrait
* REST API Controllers for appointments, types, and statuses
* Commands
    - CancelAppointment
    - MarkAppointmentAttended
    - MarkAppointmentMissed
    - RescheduleAppointment
    - ScheduleAppointment
    - UpdateAppointmentStatus
* Events
    - AppointmentAttended
    - AppointmentCancelled
    - AppointmentMissed
    - AppointmentScheduled
    - AppointmentStatusWasUpdated
    - AppointmentWasRescheduled

### Who do I talk to? ###

* TJ Ward - jward3@email.unc.edu