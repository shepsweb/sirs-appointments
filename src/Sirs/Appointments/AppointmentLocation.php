<?php 

namespace Sirs\Appointments;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class AppointmentLocation extends Model 
{
	use Sluggable;
	use SluggableScopeHelpers;

 	protected $fillable = ['id','name', 'slug'];

	public function sluggable(): array
	{
	    return [
	        'slug' => [
	            'source' => 'name'
	        ]
	    ];
	}

	public function appointments() 
	{ 
		return $this->hasMany(class_appointment());
	}

	static public function findByName($name) 
	{
		return static::where('name','=',$name)->get()->first();
	}
}
