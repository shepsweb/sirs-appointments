<?php

namespace Sirs\Appointments\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Sirs\Surveys\Documents\SurveyDocument;
use Sirs\Appointments\Contracts\AppointmentType;

class CreateWorkflowStrategy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appointments:workflow 
                            {appointment_slug : AppointmentType slug or \'all\' to create workflows for all appointment types}
                            {--replace : Replace an existing file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create AppointmentTypeWorkflowStrategy class for appointment of type';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $appointmentSlug = $this->argument('appointment_slug');
        $appointmentTypes = collect([]);
        if ($appointmentSlug == 'all') {
            $appointmentTypes = class_appointmentType::all();
        } else {
            $appointmentTypes = class_appointmentType::where('slug', $appointmentSlug)->get();
            if($appointmentTypes->count() < 1){
                $this->error('Could not find appointment type with slug '.$this->argument('appointment_slug').'.');
                $typeSlugs = class_appointmentType::all()->lists('slug')->toArray();
                $this->info("Appointment types for this project: \n\t".implode("\n\t", $typeSlugs));
                return;
            }
        }

        $appointmentTypes->each(function($type){
            $typeClassName = $this->buildClassName($type);
            $this->createClass($type);
            \Artisan::call('make:test', [
                'name' => 'Appointments/'.$typeClassName.'WorkflowStrategyTest',
                '--unit'=>true
            ]);
        });
    }

    protected function createClass(AppointmentType $appointmentType) 
    {
        $typeClassName = $this->buildClassName($appointmentType);
        $stub = file_get_contents(__DIR__.'/stubs/WorkflowStrategy.stub');
        $classContent = preg_replace('/{DummyType}/', $typeClassName, $stub);

        if (!file_exists(app_path('Appointments'))) {
            mkdir(app_path('Appointments'));
        }

        $classFile = app_path('Appointments/'.$typeClassName.'WorkflowStrategy.php');
        if (!file_exists($classFile)) {
            file_put_contents($classFile, $classContent);
            $this->info('Created class at '.$classFile);
        } elseif ($this->confirm("There's already a class for this appointment type.\n Do you want to replace the existing file? [y|N]")) {
            file_put_contents($classFile, $classContent);
            $this->info('Replaced '.$classFile);
        } else {
            $this->info('Left existing class in place.');
        }
    }

    protected function buildClassName(AppointmentType $appointmentType)
    {
        return ucfirst(camel_case($appointmentType->slug));        
    }
}