<?php

namespace Sirs\Appointments;

use Event;
use Illuminate\Support\ServiceProvider;
use Sirs\Appointments\AppointmentObserver;
use Sirs\Appointments\Console\CreateWorkflowStrategy;

class AppointmentsServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->app->bind('Sirs\Appointments\Contracts\Appointment', 'Sirs\Appointments\Appointment');
        
        // register observers:
        class_appointment()::observe(new AppointmentObserver);

        // Register RunTaskWorkflow listener
        // There has to be a better way to do this, but I can't find it.
        Event::listen('Sirs\Appointments\Events\AppointmentAttended', 'Sirs\Appointments\Handlers\RunWorkflow');
        Event::listen('Sirs\Appointments\Events\AppointmentCanceled', 'Sirs\Appointments\Handlers\RunWorkflow');
        Event::listen('Sirs\Appointments\Events\AppointmentMissed', 'Sirs\Appointments\Handlers\RunWorkflow');
        Event::listen('Sirs\Appointments\Events\AppointmentStatusWasUpdated', 'Sirs\Appointments\Handlers\RunWorkflow');
        Event::listen('Sirs\Appointments\Events\AppointmentWasRescheduled', 'Sirs\Appointments\Handlers\RunWorkflow');
        Event::listen('Sirs\Appointments\Events\AppointmentScheduled', 'Sirs\Appointments\Handlers\RunWorkflow');

        // Publish the config files
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('appointments.php'),
        ], 'config');

        // Publish the migrations
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('/migrations')
        ], 'migrations');
        
        $this->publishes([
            __DIR__.'/database/seeds/' => database_path('/seeds')
        ], 'seeds');

        // register the routes
        include __DIR__.'/routes.php';
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            CreateWorkflowStrategy::class,
        ]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    protected function bindInterfaces()
    {
        $this->app->bind(
            \Sirs\Appointments\Appointment::class, 
            config('appointments.bindings.models.Appointment', \Sirs\Appointments\Appointment::class)
        );
        $this->app->bind(
            \Sirs\Appointments\AppointmentType::class, 
            config('appointments.bindings.models.AppointmentType', \Sirs\Appointments\AppointmentType::class)
        );
        $this->app->bind(
            \Sirs\Appointments\AppointmentStatus::class, 
            config('appointments.bindings.models.AppointmentStatus', \Sirs\Appointments\AppointmentStatus::class)
        );


        $this->app->bind(
            \Sirs\Appointments\Interfaces\Appointment::class, 
            config('appointments.bindings.models.Appointment', \Sirs\Appointments\Appointment::class)
        );
        $this->app->bind(
            \Sirs\Appointments\Interfaces\AppointmentType::class, 
            config('appointments.bindings.models.AppointmentType', \Sirs\Appointments\AppointmentType::class)
        );
        $this->app->bind(
            \Sirs\Appointments\Interfaces\AppointmentStatus::class, 
            config('appointments.bindings.models.AppointmentStatus', \Sirs\Appointments\AppointmentStatus::class)
        );
    }
}