<?php

$routeGroup = (config('appointments.routeGroup')) ? config('appointments.routeGroup') : []; 
Route::group($routeGroup, function(){
    Route::group(['prefix'=>config('appointments.apiPrefix')], function(){
        // Appointments REST
        Route::resource('appointments', '\Sirs\Appointments\Controllers\RestAppointmentsController', ['as'=>'appointments']);
        Route::resource('appointment_types', '\Sirs\Appointments\Controllers\AppointmentTypesController', ['only'=>['index', 'show']]);
        Route::resource('appointment_statuses', '\Sirs\Appointments\Controllers\AppointmentStatusesController', ['only'=>['index', 'show']]);

        // These are bad because they make the REST api a REST/RPC hybrid?
        Route::get('appointments/{appointments}/rescheduleAppointment', '\Sirs\Appointments\Controllers\RestAppointmentsController@rescheduleForm');
        Route::patch('appointments/{appointments}/cancel', '\Sirs\Appointments\Controllers\RestAppointmentsController@cancel');
        Route::patch('appointments/{appointments}/reschedule', '\Sirs\Appointments\Controllers\RestAppointmentsController@reschedule');
        Route::patch('appointments/{appointments}/markAttended', '\Sirs\Appointments\Controllers\RestAppointmentsController@markAttended');
        Route::patch('appointments/{appointments}/markMissed', '\Sirs\Appointments\Controllers\RestAppointmentsController@markMissed');
    });
});
