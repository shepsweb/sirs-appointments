<?php 

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AppointmentRequest extends Request 
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'attendee_type' => 'required',
			'attendee_id' => 'required',
			'start_date' => 'required|date',
			'start_time' => 'required',
			'appointment_type_id' => 'required'
		];
	}

}
