<?php 

namespace Sirs\Appointments;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Bus;
use Sirs\Appointments\AppointmentStatus;
use Sirs\Appointments\Commands\CancelAppointment;
use Carbon\Carbon;

trait AppointmentStaffAttendeeTrait {

    public function appointments()
    {
        return $this->morphMany('Sirs\Appointments\Appointment', 'staff_attendee');
    }

    public function upcomingAppointments()
    {
        $now = new Carbon();
        return $this->appointments()->Upcoming();
    }

    public function getUpcomingAppointments()
    {
        return $this->upcomingAppointments()->get();
    }

    public function appointmentsWithStatus($appointmentStatusId)
    {
        return $this->appointments()->where('appointment_status_id','=',$appointmentStatusId);
    }

    public function getAppointmentsWithStatus($appointmentStatusId)
    {
        return $this->appointmentsWithStatus($appointmentStatusId)->get();
    }

    public function getPendingAppointments()
    {
        return $this->getAppointmentsWithStatus(AppointmentStatus::findBySlug('pending')->id);
    }

    public function getMissedAppointments()
    {
        return $this->appointmentsWithStatus(AppointmentStatus::findBySlug('missed')->id)->get();
    }

    public function getAttendedAppointments()
    {
        return $this->appointmentsWithStatus(AppointmentStatus::findBySlug('attended')->id)->get();
    }

    public function getCancelledAppointments()
    {
        return $this->appointmentsWithStatus(AppointmentStatus::findBySlug('cancelled')->id)->get();
    }
}
