<?php

namespace Sirs\Appointments;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Sirs\Appointments\Contracts\AppointmentStatus as AppointmentStatusInterface;

class AppointmentStatus extends Model implements AppointmentStatusInterface
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $fillable = ['id','name'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source'=>'name'
            ]
        ];
    }
    public function appointments()
    {
        return $this->hasMany(class_appointment());
    }

    public static function findBySlug($slug)
    {
        return static::where('slug', '=', $slug)->get()->first();
    }

    public static function findByName($name)
    {
        return static::where('name', '=', $name)->get()->first();
    }
}
