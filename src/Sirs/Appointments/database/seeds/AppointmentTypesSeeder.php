<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Sirs\Appointments\AppointmentType;

class AppointmentTypesSeeder extends Seeder 
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Model::unguard();

        foreach(config('appointments.types') as $name => $id) {
            class_appointmentType()::create([
                'id' => $id,
                'name' => $name,
                // note: that slug is set by elequent-sluggable
            ]);
        }
    }
}
