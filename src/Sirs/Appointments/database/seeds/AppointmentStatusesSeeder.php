<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AppointmentStatusesSeeder extends Seeder 
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Model::unguard();

        foreach (config('appointments.statuses') as $name => $id) {
            class_appointmentStatus()::create([
                'id' => $id,
                'name' => $name,
                // note: that slug is set by elequent-sluggable
            ]);
        }
    }
}
