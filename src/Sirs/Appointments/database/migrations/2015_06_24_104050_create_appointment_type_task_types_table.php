<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTypeTaskTypesTable extends Migration 
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointment_type_task_type', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('appointment_type_id')->unsigned();
			$table->integer('task_type_id')->unsigned();

			$table->foreign('appointment_type_id')->references('id')->on('appointment_types')->onDelete('cascade');
			$table->foreign('task_type_id')->references('id')->on('task_types')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('appointment_type_task_type');
	}
}
