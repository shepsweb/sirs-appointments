<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAppointmentLocationIdToAppointmentsTable extends Migration 
{

    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::table('appointments', function(Blueprint $table) {
            $table->integer('appointment_location_id')->unsigned()->nullable()->after('ends_at');
            $table->foreign('appointment_location_id')->references('id')->on('appointment_locations');
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::table('appointments', function(Blueprint $table) {
            $table->dropForeign('appointments_appointment_location_id_foreign');
            $table->dropColumn('appointment_location_id');
        });
    }
}
