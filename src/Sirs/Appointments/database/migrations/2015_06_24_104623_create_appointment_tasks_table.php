<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTasksTable extends Migration 
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointment_task', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('appointment_id')->unsigned();
			$table->integer('task_id')->unsigned();

			$table->foreign('appointment_id')->references('id')->on('appointments')->onDelete('cascade');
			$table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('appointment_task');
	}
}
