<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentTable extends Migration 
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('appointments', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('appointment_type_id')->unsigned();
			$table->string('attendee_type');
			$table->integer('attendee_id');
			$table->string('staff_attendee_type')->nullable();
			$table->integer('staff_attendee_id')->unsigned()->nullable();
			$table->dateTime('starts_at');
			$table->dateTime('ends_at')->nullable();
			$table->integer('appointment_status_id')->unsigned()->default(1);
			$table->mediumText('notes')->nullable();
			$table->json('data')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('appointment_type_id')->references('id')->on('appointment_types');
			$table->foreign('appointment_status_id')->references('id')->on('appointment_statuses');
			$table->index(['attendee_type','attendee_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('appointments');
	}
}
