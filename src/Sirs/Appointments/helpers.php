<?php

if (!function_exists('class_appointment')) {
	function class_appointment() {
		return config('appointments.bindings.models.Appointment', \Sirs\Appointments\Appointment::class);
	}
}

if (!function_exists('class_appointmentType')) {
	function class_appointmentType() {
		return config('appointments.bindings.models.AppointmentType', \Sirs\Appointments\AppointmentType::class);
	}
}

if (!function_exists('class_appointmentStatus')) {
	function class_appointmentStatus() {
		return config('appointments.bindings.models.AppointmentStatus', \Sirs\Appointments\AppointmentStatus::class);
	}
}
