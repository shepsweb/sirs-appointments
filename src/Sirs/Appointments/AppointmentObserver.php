<?php 

namespace Sirs\Appointments;

use Event;
use Sirs\Appointments\Contracts\Appointment;
use Sirs\Appointments\Contracts\AppointmentStatus;
use Sirs\Appointments\Contracts\AppointmnentType;
use Sirs\Appointments\Events\AppointmentAttended;
use Sirs\Appointments\Events\AppointmentCanceled;
use Sirs\Appointments\Events\AppointmentMissed;
use Sirs\Appointments\Events\AppointmentScheduled;
use Sirs\Appointments\Events\AppointmentStatusWasUpdated;
use Sirs\Appointments\Events\AppointmentWasRescheduled;


class AppointmentObserver
{
    public function __construct()
    {
        //
    }

    public function created($appointment)
    {
        Event::dispatch(new AppointmentScheduled($appointment));
    }

    public function updated($appointment)
    {
        if ($appointment->isDirty('appointment_status_id')) {
            Event::dispatch(new AppointmentStatusWasUpdated($appointment));
            switch ($appointment->appointment_status_id) {
                case config('apppintments.statuses.attended'):
                Event::dispatch(new AppointmentAttended($appointment));
                break;
                case config('appointments.statuses.missed'):
                Event::dispatch(new AppointmentMissed($appointment));
                break;
                case config('appointments.statuses.canceled'):
                Event::dispatch(new AppointmentCanceled($appointment));
                break;
                default:
                break;
            }
        }
        if ($appointment->isDirty('starts_at') || $appointment->isDirty('ends_at')) {
            Event::dispatch(new AppointmentWasRescheduled($appointment));
        }
    }

} // END class AppointmentObserver
