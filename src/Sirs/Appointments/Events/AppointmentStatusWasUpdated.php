<?php 

namespace Sirs\Appointments\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Sirs\Appointments\Events\AppointmentEvent;
use Sirs\Appointments\Contracts\Appointment;

class AppointmentStatusWasUpdated extends AppointmentEvent 
{
    use Dispatchable, SerializesModels;

    public $appointment;

    /**
    * Create a new event instance.
    *
    * @param  Appointment $appointment appointment that status was updated on
    * @return void
    */
    public function __construct(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }
}
