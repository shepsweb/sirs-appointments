<?php 

namespace Sirs\Appointments\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Sirs\Appointments\Contracts\Appointment;
use Sirs\Appointments\Events\AppointmentEvent;

class AppointmentScheduled extends AppointmentEvent 
{
	use Dispatchable, SerializesModels;
	
	public $appointment;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Appointment $appointment)
	{
		$this->appointment = $appointment;
	}
}
