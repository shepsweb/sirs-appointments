<?php 

namespace Sirs\Appointments;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sirs\Appointments\AppointmentLocation;
use Sirs\Appointments\AppointmentStatus;
use Sirs\Appointments\AppointmentType;
use Sirs\Appointments\Contracts\Appointment as AppointmentContract;

class Appointment extends Model implements AppointmentContract
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    use SoftDeletes;
  
    protected $fillable = [
        'attendee_type',
        'attendee_id',
        'staff_attendee_type',
        'staff_attendee_id',
        'starts_at',
        'ends_at',
        'appointment_type_id',
        'appointment_status_id',
        'appointment_location_id',
        'notes',
        'data',
    ];

    protected $casts = [
        'data' => 'array',
        'starts_at' => 'datetime',
        'ends_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if (config('appointments.global_scopes') && count(config('appointments.global_scopes')) > 0) {
            foreach (config('appointments.global_scopes') as $scopeClassName) {
                static::addGlobalScope(new $scopeClassName);
            }
        }
    }


    /**
     * gets tasks associated with this appointment
     *
     * @return relationship
     * @author
     **/
    public function tasks()
    {
        return $this->belongsToMany(class_task());
    }

    public function appointmentType()
    {
        return $this->belongsTo(class_appointmentType());
    }
    public function type()
    {
        return $this->appointmentType();
    }

    public function appointmentStatus()
    {
        return $this->belongsTo(class_appointmentStatus());
    }

    public function status()
    {
        return $this->appointmentStatus();
    }

    public function attendee()
    {
        return $this->morphTo();
    }

    public function staffAttendee()
    {
        return $this->morphTo();
    }

    public function location()
    {
        return $this->belongsTo(AppointmentLocation::class, 'appointment_location_id', 'id');
    }

    public function getStartDateAttribute()
    {
        return $this->starts_at->startOfDay();
    }

    public function getStartTimeAttribute()
    {
        return $this->starts_at->format('g:ia');
    }
  
    public function getPastDueAttribute()
    {
        return $this->starts_at->lt(Carbon::create()) && $this->appointment_status_id == 1;
    }

    public function scopeUpcoming($query)
    {
        $now = new Carbon();
        return $query->where('start_date', '>', $new)->where('start_time', '>', $new);
    }

    public function scopePast($query)
    {
        $now = new Carbon();
        return $query->where('start_date', '<=', $new)->where('start_time', '<', $new);
    }
}
