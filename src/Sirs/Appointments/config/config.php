<?php 
  
    return [
        'apiPrefix' => 'api',
        'routeGroup' => ['middleware' => 'auth'],
        'global_scopes' => [
            # App\Scopes\GlobalAppointmentScope::class
        ],
        'types' => [
            'Baseline' => 1,
            'Followup' => 2,
        ],
        'statuses' => [
            'scheduled' => 1,
            'attended' => 2,
            'missed' => 3,
            'canceled' => 4,
        ]
        //Override default package models here
        // 'bindings' => [
        //   'models' => [
        //     'Appointment' => \App\Appointment::class,
        //     'AppointmentType' => \App\AppointmentType::class,
        //     'AppointmentStatus' => \App\AppointmentStatus::class,
        //   ]
        // ],
    ]
