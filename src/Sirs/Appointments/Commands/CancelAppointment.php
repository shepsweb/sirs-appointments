<?php 

namespace Sirs\Appointments\Commands;

use Sirs\Appointments\Commands\AppointmentCommand;
use Sirs\Appointments\Contracts\Appointment;

class CancelAppointment extends AppointmentCommand 
{
	public $appointment;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Appointment $appointment)
	{
		$this->appointment = $appointment;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->appointment->appointment_status_id = class_appointmentStatus::findBySlug('canceled')->id;
		$this->appointment->save();
	}
}
