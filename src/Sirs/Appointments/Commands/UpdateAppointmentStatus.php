<?php 

namespace Sirs\Appointments\Commands;

use Sirs\Appointments\Commands\AppointmentCommand;
use Sirs\Appointments\Appointment;
use Sirs\Appointments\AppointmentStatus;

class UpdateAppointmentStatus extends AppointmentCommand 
{
	public $appointment;
	public $appointmentStatus;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Appointment $appointment, AppointmentStatus $appointmentStatus)
	{
		$this->appointment = $appointment;
		$this->appointmentStatus = $appointmentStatus;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$this->appointment->appointment_status_id = $this->appointmentStatus->id;
		$this->appointment->save();
	}
}
