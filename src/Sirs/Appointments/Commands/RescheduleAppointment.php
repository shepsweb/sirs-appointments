<?php 

namespace Sirs\Appointments\Commands;

use Carbon\Carbon;
use Sirs\Appointments\Commands\AppointmentCommand;
use Sirs\Appointments\Appointment;

class RescheduleAppointment extends AppointmentCommand 
{

    public $appointment;
    public $starts_at;
    public $ends_at;

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct(Appointment $appointment, $starts_at, $ends_at = null)
    {
        $this->appointment = $appointment;
        $this->starts_at = new Carbon($starts_at);
        $this->ends_at = new Carbon($ends_at);
    }

    /**
    * Execute the command.
    *
    * @return void
    */
    public function handle()
    {
        $this->appointment->starts_at = $this->starts_at;
        if (!is_null($this->ends_at)) {
            $this->appointment->ends_at = $this->ends_at;
        }

        $this->appointment->save();
    }

}
