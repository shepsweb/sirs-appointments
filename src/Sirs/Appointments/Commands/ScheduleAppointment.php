<?php 

namespace Sirs\Appointments\Commands;

use Sirs\Appointments\Commands\AppointmentCommand;

class ScheduleAppointment extends AppointmentCommand {

	public $appointment_type_id;
	public $atenddee_type;
	public $attendee_id;
	public $starts_at;
	public $ends_at;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($appointment_type_id, $attendee_type, $attendee_id, $starts_at, $ends_at=null)
	{
		$this->appointment_type_id = $appointment_type_id;
		$this->attendee_type = $attendee_type;
		$this->attendee_id = $attendee_id;
		$this->starts_at = $starts_at;
		$this->ends_at = $ends_at;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		class_appointment::firstOrCreate([
			'appointment_type_id' => $this->appointment_type_id,
			'attendee_type' => $this->attendee_type,
			'attendee_id' => $this->attendee_id,
			'starts_at' => $this->starts_at,
			'ends_at' => $this->ends_at,
			'appointment_status_id' => 1
		]);
	}
}
