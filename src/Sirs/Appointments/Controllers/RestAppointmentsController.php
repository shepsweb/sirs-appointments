<?php

namespace Sirs\Appointments\Controllers;

use Carbon\Carbon;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Redirect;
use Response;
use Sirs\Appointments\Contracts\Appointment;
use Sirs\Appointments\Contracts\AppointmentStatus;
use Sirs\Appointments\Contracts\AppointmentType;
use Sirs\Appointments\Commands\CancelAppointment;
use Sirs\Appointments\Commands\MarkAppointmentAttended;
use Sirs\Appointments\Commands\MarkAppointmentMissed;
use Sirs\Appointments\Commands\RescheduleAppointment;
use Sirs\Appointments\Commands\ScheduleAppointment;
use Sirs\Appointments\Controllers\Controller;
use Sirs\Appointments\Requests;
use Sirs\Appointments\Requests\AppointmentRequest;
use Sirs\Appointments\Requests\AppointmentRescheduleRequest;
use Validator;

class RestAppointmentsController extends Controller 
{
    protected $validFilters = [
        'appointment_type_id',
        'appointment_status_id',
        'attendee_id',
        'attendee_type'
    ];

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $query = class_appointment()::with(
            'status', 
            'type', 
            'attendee', 
            'tasks',
            'tasks.taskType',
            'location',
            'staffAttendee'
        );

        foreach ($request->all() as $field => $value) {
            if (in_array($field, $this->validFilters)) {
                $query->where($field,'=',$value);
            }

            if ($field == 'range_start') {
                $query->where('starts_at', '>=', $value);
            }

            if ($field == 'range_end') {
                $query->where('starts_at', '<=', $value);
            }
        }
        $count = $query->count();

        if ($order = $request->query('orderby')) {
            list($orderField, $orderDir) = preg_split('/[ ,]/', $order);
        } else {
            $orderField = 'starts_at';
            $orderDir = 'asc';
        }

        $query->orderBy($orderField, $orderDir);

        if ($limit = $request->query('limit')) {
            $query->take($limit);
        }
        if ($offset = $request->query('offset')) {
            $query->skip($offset);
        }

        $items = $query->get();
        return $items;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return response('Not implemented', 501);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $validation = Validator::make($request->all(),[ 
            'attendee_type' => 'required',
            'attendee_id' => 'required',
            'starts_at' => 'required|date|after:yesterday',
            'appointment_type_id' => 'required'
        ]);

        if($validation->fails()){
            return Response::json([
                'status' => 'error',
                'errors' => $validation->errors()
            ]);
        }	

        Bus::dispatch(new ScheduleAppointment(), $request->all());
    
        return Response::json([
            'status' => 'success',
            'messages' => ['Appointment scheduled']
        ]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $appointment = class_appointment()::where('id',$id)->with(
            'status', 
            'type', 
            'attendee', 
            'tasks',
            'tasks.taskType',
            'location',
            'staffAttendee'
        )->first();
        return $appointment;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return response('Not implemented', 501);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Appointment $appointment, Request $request)
	{
        $validation = Validator::make($request->all(),[ 
            'attendee_type'=>'required',
            'attendee_id'=>'required',
            'starts_at'=>'required|date|after:yesterday',
            'appointment_type_id'=>'required'
        ]);

        if ($validation->fails()) {
            return Response::json([
                'status'=>'error',
                'errors'=>$validation->errors()
            ]);
        } 

        try {
            // throw new \Exception('test Exception');
            $appointment->update($request->all());
            return Response::json([
                'status' => 'success',
                'messages' => [
                    'Appointment '.$appointment->id.' updated'
                ]
            ]);
        } catch (\Exception $e) {
            return Response::json([
                'status' => 'error',
                'messages' => [
                    'Error '.$e->getCode().': '.$e->getMessage(),
                ]
            ], 500);
        }
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Appointment $appointment)
	{
        try {
            $appointment->delete();
            return Response::json([
                'status' => 'success',
                'messages' => [
            	   'Appointment '.$appointment->id.' deleted'
                ]
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 'error',
                'messages' => [
                    $e->getCode().': '.$e->getMessage()
                ], 500
            ]);
        }
	}

	/**
	 * Mark the appointment attended at redirect to the list
	 *
	 * @return void
	 * @author TJ Ward
	 **/
	public function reschedule(Appointment $appointment, Request $request)
	{
        $validation = Validator::make($request->all(),[ 
            'starts_at' => 'required|date|after:yesterday',
        ]);

        if($validation->fails()){
            return Response::json([
                'status' => 'error',
                'errors' => $validation->errors()
            ]);
        } 

        try {
            $this->dispatch(new RescheduleAppointment($appointment, $request->get('starts_at'), $request->get('ends_at')));
            return Response::json([
                'status' => 'success',
                'messages' => [
                    'Appointment rescheduled.'
                ]
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 'error',
                'messages' => [
                    $e->getCode().': '.$e->getMessage()
                ], 500
            ]);
        }
	}

	/**
	 * Mark the appointment attended at redirect to the list
	 *
	 * @return void
	 * @author TJ Ward
	 **/
	public function markAttended(Appointment $appointment)
	{
        try {
            $this->dispatch(new MarkAppointmentAttended($appointment));
            return Response::json([
                'status' => 'success',
                'messages' => [
                    'Appointment marked attended.'
                ]
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 'error',
                'messages' => [
                    $e->getCode().': '.$e->getMessage()
                ], 500
            ]);
        }
	}

	/**
	 * Mark the appointment missed at redirect to the list
	 *
	 * @return void
	 * @author TJ Ward
	 **/
	public function markMissed(Appointment $appointment)
	{
        try {
            $this->dispatch(new MarkAppointmentMissed($appointment));
            return Response::json([
                'status' => 'success',
                'messages' => [
                'Appointment marked missed.'
                ]
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 'error',
                'messages' => [
                    $e->getCode().': '.$e->getMessage()
                ], 500
            ]);
        }
	}

	/**
	 * Mark the appointment canceled at redirect to the list
	 *
	 * @return void
	 * @author TJ Ward
	 **/
	public function cancel(Appointment $appointment)
	{
        try {
            $this->dispatch(new CancelAppointment($appointment));
            return Response::json([
                'status' => 'success',
                'messages' => [
                    'Appointment canceled'
                ]
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 'error',
                'messages' => [
                    $e->getCode().': '.$e->getMessage()
                ], 500
            ]);
        }
	}

	public function types(Request $request)
	{
        $validFilters = ['id', 'name', 'slug'];
        $query = class_appointmentType()::query();
        foreach($request->all() as $field => $value) {
            if(in_array($field, $validFilters)) {
                $query->where($field, '=' ,$value);
            }
        }
        $count = $query->count();

        if ($limit = $request->query('limit')) {
            $query->take($limit);
        }
        if ($offset = $request->query('offset')) {
            $query->skip($offset);
        }

        $items = $query->get();

        return Response::json([
            'total' => $count,
            'items' => $items
        ], 200);
	}

	public function statuses(Request $request)
	{
        $validFilters = ['id', 'name', 'slug'];
        $query = class_appointmentStatus()::query();
        foreach($request->all() as $field => $value) {
            if (in_array($field, $validFilters)) {
                $query->where($field, '=' ,$value);
            }
        }
        $count = $query->count();

        if ($limit = $request->query('limit')) {
            $query->take($limit);
        }
        if ($offset = $request->query('offset')) {
            $query->skip($offset);
        }

        $items = $query->get();

        return Response::json([
            'total' => $count,
            'items' => $items
        ], 200);
	}
}
