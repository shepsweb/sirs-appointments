<?php

namespace Sirs\Appointments\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Sirs\Appointments\Contracts\AppointmentType;
use Sirs\Appointments\Controllers\Controller;

class AppointmentTypesController extends Controller 
{
    protected $validFilters = [
        'id',
        'name',
        'slug',
    ];

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
    public function index(Request $request)
    {
        $query = class_appointmentType()::query();
        foreach ($request->all() as $field => $value) {
            if (in_array($field, $this->validFilters)) {
                $query->where($field, '=' ,$value);
            }
        }
        $count = $query->count();

        if ($limit = $request->query('limit')) {
            $query->take($limit);
        }
        if ($offset = $request->query('offset')) {
            $query->skip($offset);
        }

        $items = $query->get();

        return Response::json([
            'total' => $count,
            'items' => $items
        ], 200);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $appointmentType = class_appointmentType()::where('id', $id)->first();
        $item = $appointmentType ? $this->transform($appointmentType) : null;
        return Response::json([
          'item' => $item
        ]);
    }

    protected function transformCollection(Collection $appointmentTypes)
    {
        return $appointmentTypes->map(function (AppointmentType $appointmentType) {
            return $this->transform($appointment);
        });
    }

    protected function transform(AppointmentType $appointmentType)
    {
        return [
            'id' => $appointmentType->id,
            'name' => $appointmentType->name,
            'slug' => $appointmentType->slug,
        ];
    }
}
