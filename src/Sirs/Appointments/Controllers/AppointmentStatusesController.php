<?php

namespace Sirs\Appointments\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Sirs\Appointments\Contracts\Appointment;
use Sirs\Appointments\Contracts\AppointmentStatus;
use Sirs\Appointments\Controllers\Controller;

class AppointmentStatusesController extends Controller 
{
    protected $validFilters = [
        'id',
        'name',
        'slug',
    ];

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index(Request $request)
    {
        $query = class_appointmentStatus()::query();
        foreach($request->all() as $field => $value) {
            if (in_array($field, $this->validFilters)) {
                $query->where($field, '=' ,$value);
            }
        }
        $count = $query->count();

        if ($limit = $request->query('limit')) {
            $query->take($limit);
        }
        if ($offset = $request->query('offset')) {
            $query->skip($offset);
        }

        $items = $query->get();

        return Response::json([
          'total' => $count,
          'items' => $items
        ], 200);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {
        $appointmentStatus = class_appointmentStatus()::where('id', $id)->first();
        $item = $appointmentStatus ? $this->transform($appointmentStatus) : null;
        return Response::json([
          'item' => $item
        ]);
    }

    protected function transformCollection(Collection $appointmentStatuses)
    {
        return $appointmentStatuses->map(function (AppointmentStatus $appointmentStatus) {
            return $this->transform($appointment);
        });
    }

    protected function transform(AppointmentStatus $appointmentStatus)
    {
        return [
            'id' => $appointmentStatus->id,
            'name' => $appointmentStatus->name,
            'slug' => $appointmentStatus->slug,
        ];
    }
}
