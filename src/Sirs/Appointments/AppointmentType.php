<?php

namespace Sirs\Appointments;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Sirs\Appointments\Contracts\AppointmentType as AppointmentTypeInterface;

class AppointmentType extends Model implements AppointmentTypeInterface
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $fillable = ['id','name'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source'=>'name'
            ]
        ];
    }

    public function taskTypes()
    {
        return $this->belongsToMany(class_taskType());
    }

    public function appointments()
    {
        return $this->hasMany(class_Appointment());
    }

    public static function findBySlug($slug)
    {
        return static::where('slug', '=', $slug)->get()->first();
    }

    public static function findByName($name)
    {
        return static::where('name', '=', $name)->get()->first();
    }

    public static function getOptionsArray()
    {
        return static::select('id', 'name')->get()->keyBy('id')->transform(function ($item) {
            return $item->name;
        });
    }
}
