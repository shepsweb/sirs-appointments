<?php

namespace Sirs\Appointments;

use Sirs\Appointments\Events\AppointmentEvent;
use Sirs\Appointments\Contracts\WorkflowStrategy;
use Sirs\Appointments\Appointment;

/**
 * A strategy for handling all events for a given AppointmentType
 *
 * @package Sirs\Appointments
 **/
class AppointmentTypeWorkflowStrategy implements WorkflowStrategy
{
    protected $appointment;
    protected $event;

    /**
     * Constructor
     * @param Appointment      $appointment  The Appointment for which the AppointmentEvent was fired
     * @param Events\AppointmentEvent $event
     */
    public function __construct(Appointment $appointment, AppointmentEvent $event){
        $this->appointment = $appointment;
        $this->event = $event;
    }

    /**
     * Determines and executes the appropriate instance method based on AppointmentEvent type.
     * @return void 
     */
    public function run()
    {
        $reflection = new \ReflectionClass(get_class($this->event));

        $method = lcfirst(substr($reflection->getShortName(), strlen('Appointment')));
        
        if (method_exists($this, $method)) {
            $this->$method($this->appointment);
        }
    }

    public function attended()
    {
    }

    public function canceled()
    {
    }

    public function missed()
    {
    }

    public function scheduled()
    {
    }

    public function wasRescheduled()
    {
    }

    public function statusWasUpdated()
    {
    }

} // END class AppointmentTypeWorkflowStrategy
