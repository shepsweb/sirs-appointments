<?php 

namespace Sirs\Appointments;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Bus;
use Sirs\Appointments\AppointmentStatus;
use Sirs\Appointments\Commands\CancelAppointment;
use Carbon\Carbon;

trait AppointmentAttendeeTraits {

	public function appointments()
	{
		return $this->morphMany(class_appointment(), 'attendee');
	}

	public function upcomingAppointments()
	{
		$now = new Carbon();
		return $this->appointments()->Upcoming();
	}

	public function getUpcomingAppointments()
	{
		return $this->upcomingAppointments()->get();
	}


	public function appointmentsWithStatus($appointmentStatusId)
	{
		return $this->appointments()->where('appointment_status_id','=',$appointmentStatusId);
	}

	public function getAppointmentsWithStatus($appointmentStatusId)
	{
		return $this->appointmentsWithStatus($appointmentStatusId)->get();
	}

	public function getPendingAppointments()
	{
		return $this->getAppointmentsWithStatus(class_appointmentStatus()::findBySlug('pending')->id);
	}

	public function getMissedAppointments()
	{
		return $this->appointmentsWithStatus(class_appointmentStatus()::findBySlug('missed')->id)->get();
	}

	public function getAttendedAppointments()
	{
		return $this->appointmentsWithStatus(class_appointmentStatus()::findBySlug('attended')->id)->get();
	}

	public function getCancelledAppointments()
	{
		return $this->appointmentsWithStatus(class_appointmentStatus()::findBySlug('cancelled')->id)->get();
	}

	public function cancelPendingAppointments()
	{
		foreach ($this->getPendingAppointments() as $appt) {
			Bus::dispatch(new CancelAppointment($appt));
		}
	}
}
