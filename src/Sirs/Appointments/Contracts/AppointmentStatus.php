<?php

namespace Sirs\Appointments\Contracts;

interface AppointmentStatus
{
    public function sluggable();
    public function appointments();
    public static function findBySlug($slug);
    public static function findByName($name);
}
