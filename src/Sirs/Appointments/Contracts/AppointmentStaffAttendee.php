<?php

namespace Sirs\Appointments\Contracts;

/**
 * Defines interface all task owners must adhere to.
 *
 * @package default
 * @author 
 **/
interface AppointmentStaffAttendee
{
    public function fullName();
    public function appointments();
    public function upcomingAppointments();
    public function getUpcomingAppointments();
    public function appointmentsWithStatus($appointmentStatusId);
    public function getAppointmentsWithStatus($appointmentStatusId);
    public function getPendingAppointments();
    public function getMissedAppointments();
    public function getAttendedAppointments();
    public function getCancelledAppointments();
} // END interface OwnerInterface
