<?php 

namespace Sirs\Appointments\Contracts;

use Carbon\Carbon;

/**
 * Defines interface all appointments must adhear to.
 *
 * @package default
 * @author 
 **/
interface Appointment
{
    public function save();
}