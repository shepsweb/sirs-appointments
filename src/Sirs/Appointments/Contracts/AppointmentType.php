<?php

namespace Sirs\Appointments\Contracts;
/**
 * Interface for Appointment type models
 *
 **/
interface AppointmentType
{
    public function sluggable();
    public function taskTypes();
    public function appointments();
    public static function findBySlug($slug);
    public static function findByName($name);
    public static function getOptionsArray();
}
