<?php 

namespace Sirs\Appointments\Contracts;

/**
 * Defines interface for task workflow strategies.
 *
 * @package Sirs\Appointments
 **/
interface WorkflowStrategy
{
    /*
     * Constructor
     * @param Sirs\Appointments\Appointment $appointment
     * @param Sirs\Appointments\Events\AppointmentEvent $event
     */
    public function __construct(\Sirs\Appointments\Appointment $appointment, \Sirs\Appointments\Events\AppointmentEvent $event);

    /*
     * runs the appropriate method based on $this->task and $this->event
     */ 
    public function run();

    public function attended();

    public function canceled();

    public function missed();

    public function scheduled();

    public function wasRescheduled();

    public function statusWasUpdated();
} // END interface AppointmentWorkflowStrategyInterface
