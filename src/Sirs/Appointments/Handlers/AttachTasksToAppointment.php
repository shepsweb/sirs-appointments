<?php

namespace Sirs\Appointments\Handlers;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Support\Facades\Bus;

class StartParentTask 
{
    /**
    * Create the event handler.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }

    /**
    * Handle the event.
    *
    * @param  Events  $event
    * @return void
    */
    public function handle(AppointmentScheduled $event)
    {
        $appointment = $event->appointment;
    }
}
