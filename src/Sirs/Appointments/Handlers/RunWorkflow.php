<?php

namespace Sirs\Appointments\Handlers;

use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Bus;
use Sirs\Appointments\Events\AppointmentEvent;

/**
 * Event Listener that calls appropriate workflow strategy if it exists
 */
class RunWorkflow 
{
    /**
    * Create the event handler.
    *
    * @return void
    */
    public function __construct()
    {
        //
    }

    /**
    * Handle the event.
    *
    * @param  Sirs\Appointments\Events\AppointmentEvent  $event
    * @return void
    */
    public function handle(AppointmentEvent $event)
    {
        $appointment = $event->appointment;

        $workflowClassName = 'App\\Appointments\\'.ucfirst(camel_case($appointment->AppointmentType->slug).'WorkflowStrategy');
        if (class_exists($workflowClassName)) {
            $workflow = new $workflowClassName($appointment, $event);
            $workflow->run();
        }
    }
}
