<?php

namespace spec\Sirs\Appointments\Commands;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UpdateAppointmentStatusSpec extends ObjectBehavior
{
    function let(Appointment $appointment){
      $appointment->beADoubleOf('\Sirs\Appointments\Contracts\Appointment');
      $this->beConstructedWith($appointment);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Sirs\Appointments\Commands\UpdateAppointmentStatus');
    }
}
